# Siam Piwat Backend API Challenge 2.1

By. Kanut Warangwonghirun

## Run your application

1. Install
- [JAVA](https://www.oracle.com/java/technologies/downloads/)
- [Maven](https://maven.apache.org/install.html)
2. go to "siam-piwat" directory
3. run command

```bash
mvn spring-boot:run
```

## Other command

Compile the code

```bash
mvn compile
```

Make JAR file

```bash
mvn package
```

Run your application (JAR file)

- go to "siam-piwat" directory
```bash
java -jar target/demo-0.0.1-SNAPSHOT.jar
```