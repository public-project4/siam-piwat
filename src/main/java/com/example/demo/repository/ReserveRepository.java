package com.example.demo.repository;

import com.example.demo.entity.Table;
import lombok.Data;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Data
public class ReserveRepository {
    private Map<Long, List<Table>> reserveList = new HashMap<>();

    public Long getBookingId(){
        long bookingId;
        if (reserveList.size() == 0) {
            bookingId = 1L;
        } else {
            Map.Entry<Long,List<Table>> lastEntry = reserveList.entrySet().stream().reduce((first,second) -> second).get();
            bookingId = lastEntry.getKey() + 1;
        }
        return bookingId;
    }

    public void add(Long bookingId, List<Table> reserves) {
        reserveList.put(bookingId, reserves);
    }

    public void cancel(Long bookingId) {
        reserveList.remove(bookingId);
    }

    public List<Table> getById(Long bookingId) {
        return reserveList.get(bookingId);
    }
}
