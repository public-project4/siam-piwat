package com.example.demo.repository;

import com.example.demo.entity.Table;
import lombok.Data;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@Data
public class TableRepository {
    private Map<Integer, Integer> tables;

    public void initialTable(Table table){
        tables.put(table.getSeat(), table.getAmount());
    }

    public Integer getById(Integer seat){
        return tables.get(seat);
    }

    public List<Table> getAll(){
        List<Table> tables = new ArrayList<>();
        for (Map.Entry<Integer,Integer> table : this.tables.entrySet()){
            tables.add(new Table(table.getKey(), table.getValue()));
        }
        return tables;
    }

    public Table decreaseRemaining(Integer seat, Integer amount){
        Integer existing = getById(seat);
        Integer remaining = existing - amount;
        tables.replace(seat, remaining);
        return new Table(seat, remaining);
    }

    public Table increaseRemaining(Integer seat, Integer amount){
        Integer existing = getById(seat);
        Integer remaining = existing + amount;
        tables.replace(seat, remaining);
        return new Table(seat, remaining);
    }

}
