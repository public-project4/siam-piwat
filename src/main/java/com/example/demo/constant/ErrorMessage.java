package com.example.demo.constant;

import org.springframework.stereotype.Component;

@Component
public final class ErrorMessage {

    private ErrorMessage(){

    }

    public static final String ERROR_DUPLICATED_CODE = "BE0001";
    public static final String ERROR_DUPLICATED_MSG = "! Duplicated: The %d seats table has been initialized.";
    public static final String ERROR_NOT_FOUND_CODE = "BE0002";
    public static final String ERROR_NOT_FOUND_MSG = "! Not found: The initialized table was not found.";
    public static final String ERROR_NOT_ENOUGH_CODE = "BE0003";
    public static final String ERROR_NOT_ENOUGH_MSG = "! Not enough table.";

    public static final String ERROR_ID_NOT_FOUND_CODE = "BE0004";
    public static final String ERROR_ID_NOT_FOUND_MSG = "! Not found: Booking ID %d";
}
