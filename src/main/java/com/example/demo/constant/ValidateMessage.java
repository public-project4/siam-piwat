package com.example.demo.constant;

import org.springframework.stereotype.Component;

@Component
public final class ValidateMessage {

    private ValidateMessage(){

    }

    public static final String VALIDATE_ERROR_CODE = "VL0001";
    public static final String VALIDATE_MIN_MSG = "! %s must not be less than %d .";
}
