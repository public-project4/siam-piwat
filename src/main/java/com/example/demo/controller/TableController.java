package com.example.demo.controller;

import com.example.demo.dto.*;
import com.example.demo.service.ReserveService;
import com.example.demo.service.TableService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/table")
public class TableController {

    final TableService tableService;
    final ReserveService reserveService;

    @PutMapping("/initial")
    ResponseEntity<Void> initial(@RequestBody TableRequest request){
        tableService.initial(request);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/reserve")
    ResponseEntity<ReserveResponse> reserve(@RequestBody ReserveRequest request) {
        ReserveResponse response = reserveService.reserve(request);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/reserve/cancel")
    ResponseEntity<ReserveCancelResponse> reserveCancel(@RequestBody ReserveCancelRequest request) {

        ReserveCancelResponse response = reserveService.cancel(request);

        return ResponseEntity.ok(response);
    }
}
