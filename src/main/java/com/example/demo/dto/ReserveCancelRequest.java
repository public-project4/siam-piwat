package com.example.demo.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReserveCancelRequest {
    @NonNull
    private Long bookingId;
}
