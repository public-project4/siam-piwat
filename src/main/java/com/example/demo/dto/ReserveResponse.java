package com.example.demo.dto;

import com.example.demo.entity.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReserveResponse {
    private Long bookingId;
    private List<Table> reserve;
    private List<Table> remaining;
}
