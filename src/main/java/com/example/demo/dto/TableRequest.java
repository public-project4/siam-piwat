package com.example.demo.dto;

import com.example.demo.constant.ValidateMessage;
import com.example.demo.exception.ValidateException;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TableRequest {
    List<Table> tableList;

    @Data
    @AllArgsConstructor
    public static class Table {
        @NonNull
        private Integer seat;
        @NonNull
        private Integer amount;
    }

    public void validate(){
        int min = 1;
        for (Table table : tableList) {
            if (table.amount < min) {
                throw new ValidateException(ValidateMessage.VALIDATE_ERROR_CODE, String.format(ValidateMessage.VALIDATE_MIN_MSG, "amount", min));
            }
        }

    }
}

