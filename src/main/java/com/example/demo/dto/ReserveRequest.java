package com.example.demo.dto;

import com.example.demo.constant.ValidateMessage;
import com.example.demo.exception.ValidateException;
import lombok.*;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.Min;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
public class ReserveRequest {
    @NonNull
    private Integer customer;
    private List<Integer> expectSeat;

    public void validate(){
        int min = 1;
        if (customer < min) {
            throw new ValidateException(ValidateMessage.VALIDATE_ERROR_CODE, String.format(ValidateMessage.VALIDATE_MIN_MSG, "customer", min));
        }
    }
}
