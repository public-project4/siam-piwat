package com.example.demo.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Log4j2
public class ExceptionHandle extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessExceptions(BusinessException exception) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(exception.getCode());
        responseError.setErrMessage(exception.getMessage());
        log.warn(exception.getMessage());
        ResponseEntity<Object> entity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        return entity;
    }

    @ExceptionHandler(ValidateException.class)
    public ResponseEntity<Object> handleValidateExceptions(ValidateException exception) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(exception.getCode());
        responseError.setErrMessage(exception.getMessage());
        log.warn(exception.getMessage());
        ResponseEntity<Object> entity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        return entity;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleExceptions(Exception exception) {
        ResponseError responseError = new ResponseError();
        responseError.setCode("ERR9999");
        responseError.setErrMessage("something went wrong");
        log.error( "Exception!", exception);
        ResponseEntity<Object> entity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        return entity;
    }
}
