package com.example.demo.exception;

import lombok.Data;

@Data
public class ResponseError {
    private String code;
    private String errMessage;
}
