package com.example.demo.exception;

import lombok.Data;

@Data
public class BusinessException extends RuntimeException{
    private final String code;
    private final String message;
    public BusinessException(String code, String message){
        this.code = code;
        this.message = message;
    }


}
