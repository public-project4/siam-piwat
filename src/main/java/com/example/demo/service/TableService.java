package com.example.demo.service;

import com.example.demo.constant.Constant;
import com.example.demo.constant.ErrorMessage;
import com.example.demo.dto.TableRequest;
import com.example.demo.entity.Table;
import com.example.demo.exception.BusinessException;
import com.example.demo.repository.TableRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Log4j2
public class TableService {

    final TableRepository tableRepository;
    public void initial (TableRequest request) {
        try{
            request.validate();

            Map<Integer,Integer> tables = tableRepository.getTables();
            if (tables == null) {
                tables = new HashMap<>();
                tableRepository.setTables(tables);
            }

            for (TableRequest.Table initTable : request.getTableList()) {
                // get table type
                if (tables.get(initTable.getSeat()) == null){

                    Table table = new Table();
                    BeanUtils.copyProperties(initTable, table);

                    tableRepository.initialTable(table);

                    log.info(String.format("Initial %d seats %d tables completed", initTable.getSeat(), initTable.getAmount()));
                } else {
                    throw new BusinessException(ErrorMessage.ERROR_DUPLICATED_CODE, String.format(ErrorMessage.ERROR_DUPLICATED_MSG, initTable.getSeat()));
                }
            }
        } catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

}
