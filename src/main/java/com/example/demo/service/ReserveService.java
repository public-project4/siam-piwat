package com.example.demo.service;

import com.example.demo.constant.Constant;
import com.example.demo.constant.ErrorMessage;
import com.example.demo.dto.ReserveCancelRequest;
import com.example.demo.dto.ReserveCancelResponse;
import com.example.demo.dto.ReserveRequest;
import com.example.demo.dto.ReserveResponse;
import com.example.demo.entity.Table;
import com.example.demo.exception.BusinessException;
import com.example.demo.repository.ReserveRepository;
import com.example.demo.repository.TableRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@RequiredArgsConstructor
@Log4j2
public class ReserveService {

    final TableRepository tableRepository;
    final ReserveRepository reserveRepository;

    public ReserveResponse reserve (ReserveRequest request) {
        try{
            request.validate();

            Map<Integer,Integer> tables = checkInitialTable();
            List<Integer> expectSeatList = new ArrayList<>();
            if (CollectionUtils.isEmpty(request.getExpectSeat())){
                for (Map.Entry<Integer, Integer> seat : tables.entrySet()){
                    expectSeatList.add(seat.getKey());
                }
            } else {
                expectSeatList = request.getExpectSeat();
            }

            expectSeatList.sort(Collections.reverseOrder());

            int customer = request.getCustomer();
            int restCustomer = -1;

            List<Table> reserveList = new ArrayList<>();

            for (Integer expectSeat : expectSeatList) {
                if ( customer > expectSeat || restCustomer > expectSeat) {
                    restCustomer = customer % expectSeat;
                }

                int seater = customer > 0 ? customer : restCustomer;

                Integer availableTables = tables.get(expectSeat);
                if (availableTables == null || availableTables < 1) { continue; }

                Integer reserveTables = (int) Math.ceil(seater / expectSeat.doubleValue());

                if (reserveTables > availableTables) {
                     reserveTables = availableTables;
                     customer -= restCustomer;
                } else {
                    restCustomer = 0;
                }

                reserveList.add(new Table(expectSeat, reserveTables));
                customer -= (expectSeat * reserveTables);

                if ((customer + restCustomer) >= expectSeat ) {
                    customer += restCustomer;
                    restCustomer = 0;
                }
            }

            if ( customer > 0 || restCustomer > 0 ){
                throw new BusinessException(ErrorMessage.ERROR_NOT_ENOUGH_CODE, ErrorMessage.ERROR_NOT_ENOUGH_MSG);
            }

            List<Table> remainingList = decreaseRemaining(reserveList);

            Long bookingId = reserveRepository.getBookingId();

            ReserveResponse response = ReserveResponse.builder()
                .bookingId(bookingId)
                .reserve(reserveList)
                .remaining(remainingList)
                .build();

            reserved(response);

            return response;
        }catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

    public ReserveCancelResponse cancel (ReserveCancelRequest request) {
        try{
            checkInitialTable();

            Long bookingId = request.getBookingId();

            List<Table> reserveTable = reserveRepository.getById(bookingId);

            if (reserveTable == null) {
                throw new BusinessException(ErrorMessage.ERROR_ID_NOT_FOUND_CODE, String.format(ErrorMessage.ERROR_ID_NOT_FOUND_MSG, bookingId));
            }

            reserveRepository.cancel(bookingId);

            List<Table> remainingList = increaseRemaining(reserveTable);

            return ReserveCancelResponse.builder()
                .bookingId(bookingId)
                .reserveCancel(reserveTable)
                .remaining(remainingList)
                .build();

        }catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

    private List<Table> decreaseRemaining(List<Table> reserveList){
        try{
            List<Table> remainingList = new ArrayList<>();
            for (Table reserve : reserveList){
                Table remaining = tableRepository.decreaseRemaining(reserve.getSeat(), reserve.getAmount());
                remainingList.add(remaining);
            }
            return remainingList;
        }catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

    private List<Table> increaseRemaining(List<Table> reserveCancelList){
        try{
            List<Table> remainingList = new ArrayList<>();
            for (Table reserveCancel : reserveCancelList){
                Table remaining = tableRepository.increaseRemaining(reserveCancel.getSeat(), reserveCancel.getAmount());
                remainingList.add(remaining);
            }
            return remainingList;
        }catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

    private void reserved(ReserveResponse reserve){
        try{
            reserveRepository.add(reserve.getBookingId(), reserve.getReserve());
        }catch (Exception e){
            log.warn(Constant.ERROR_OCCURRED);
            throw e;
        }
    }

    private Map<Integer,Integer> checkInitialTable(){
        Map<Integer,Integer> tables = tableRepository.getTables();
        if (tables == null) {
            throw new BusinessException(ErrorMessage.ERROR_NOT_FOUND_CODE, ErrorMessage.ERROR_NOT_FOUND_MSG);
        }
        return tables;
    }

}


