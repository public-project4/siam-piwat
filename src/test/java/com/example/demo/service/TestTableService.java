package com.example.demo.service;

import com.example.demo.dto.TableRequest;
import com.example.demo.exception.BusinessException;
import com.example.demo.exception.ValidateException;
import com.example.demo.repository.TableRepository;
import org.junit.Test;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestTableService {

    @Mock
    TableRepository tableRepository;
    @InjectMocks
    TableService tableService;

    @Test(expected = BusinessException.class)
    public void Test_Initial_Duplicate(){
        Map<Integer, Integer> map = Map.of(4,5);
        when(tableRepository.getTables()).thenReturn(map);

        tableService.initial(new TableRequest(List.of(new TableRequest.Table(4,5))));
    }

    @Test(expected = ValidateException.class)
    public void Test_Initial_ZeroAmount(){

        tableService.initial(new TableRequest(List.of(new TableRequest.Table(4,0))));
    }

    @Test()
    public void Test_Initial(){
        when(tableRepository.getTables()).thenReturn(null);

        tableService.initial(new TableRequest(List.of(new TableRequest.Table(4,5))));

        verify(tableRepository).initialTable(any());
    }

    @Test(expected = Exception.class)
    public void Test_Reserve_ErrorFromRepository(){
        doThrow(Exception.class).when(tableRepository).initialTable(any());

        tableService.initial(new TableRequest(List.of(new TableRequest.Table(4,5))));

    }

}
