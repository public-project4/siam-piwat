package com.example.demo.service;

import com.example.demo.dto.ReserveCancelRequest;
import com.example.demo.dto.ReserveCancelResponse;
import com.example.demo.dto.ReserveRequest;
import com.example.demo.dto.ReserveResponse;
import com.example.demo.entity.Table;
import com.example.demo.exception.BusinessException;
import com.example.demo.exception.ValidateException;
import com.example.demo.repository.ReserveRepository;
import com.example.demo.repository.TableRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestReserveService {

    @Mock
    TableRepository tableRepository;
    @Mock
    ReserveRepository reserveRepository;
    @InjectMocks
    ReserveService reserveService;

    @Test(expected = BusinessException.class)
    public void Test_Reserve_NoData(){
        when(tableRepository.getTables()).thenReturn(null);

        reserveService.reserve(new ReserveRequest(2));
    }

    @Test(expected = ValidateException.class)
    public void Test_Reserve_ZeroCustomer(){
        reserveService.reserve(new ReserveRequest(0));
    }

    @Test
    public void Test_Reserve(){
        Map<Integer, Integer> map = Map.of(4,5);
        when(tableRepository.getTables()).thenReturn(map);
        when(reserveRepository.getBookingId()).thenReturn(1L);
        when(tableRepository.decreaseRemaining(4, 3)).thenReturn(new Table(4,2));

        ReserveResponse response = reserveService.reserve(new ReserveRequest(10));

        Assert.assertNotNull(response.getBookingId());
        Assert.assertEquals(Integer.valueOf(3), response.getReserve().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getReserve().get(0).getSeat());
        Assert.assertEquals(Integer.valueOf(2), response.getRemaining().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getRemaining().get(0).getSeat());

    }

    @Test
    public void Test_Reserve_MultipleTableType(){
        Map<Integer, Integer> map = Map.of(4,5, 5, 1);
        when(tableRepository.getTables()).thenReturn(map);
        when(reserveRepository.getBookingId()).thenReturn(1L);
        when(tableRepository.decreaseRemaining(5, 1)).thenReturn(new Table(5,0));
        when(tableRepository.decreaseRemaining(4, 2)).thenReturn(new Table(4,3));
        List<Integer> expectTableType = Arrays.asList(4,5);
        ReserveResponse response = reserveService.reserve(new ReserveRequest(12, expectTableType));

        Assert.assertNotNull(response.getBookingId());
        Assert.assertEquals(Integer.valueOf(1), response.getReserve().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(5), response.getReserve().get(0).getSeat());
        Assert.assertEquals(Integer.valueOf(2), response.getReserve().get(1).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getReserve().get(1).getSeat());
        Assert.assertEquals(Integer.valueOf(0), response.getRemaining().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(5), response.getRemaining().get(0).getSeat());
        Assert.assertEquals(Integer.valueOf(3), response.getRemaining().get(1).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getRemaining().get(1).getSeat());
    }

    @Test(expected = BusinessException.class)
    public void Test_Reserve_NotEnough(){
        Map<Integer, Integer> map = Map.of(4,5);
        when(tableRepository.getTables()).thenReturn(map);

        reserveService.reserve(new ReserveRequest(22));

    }

    @Test(expected = Exception.class)
    public void Test_Reserve_ErrorFromRepository(){
        when(reserveRepository.getBookingId()).thenThrow(Exception.class);
        reserveService.reserve(new ReserveRequest(5));
    }

    @Test
    public void Test_ReserveCancel(){
        when(tableRepository.increaseRemaining(4,2)).thenReturn(new Table(4,5));

        List<Table> reserveTable = List.of(new Table(4, 2));
        when(reserveRepository.getById(1L)).thenReturn(reserveTable);
        ReserveCancelResponse response = reserveService.cancel(new ReserveCancelRequest(1L));

        Assert.assertNotNull(response.getBookingId());
        Assert.assertEquals(Integer.valueOf(2), response.getReserveCancel().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getReserveCancel().get(0).getSeat());
        Assert.assertEquals(Integer.valueOf(5), response.getRemaining().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(4), response.getRemaining().get(0).getSeat());
    }

    @Test(expected = BusinessException.class)
    public void Test_ReserveCancel_IdNotFound(){
        when(reserveRepository.getById(1L)).thenReturn(null);
        reserveService.cancel(new ReserveCancelRequest(1L));
    }


}
