package com.example.demo.integration;

import com.example.demo.dto.*;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestTableController {
    @LocalServerPort
    private int port;
    private Gson gson = new Gson();
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Before
    public void setHeaders(){
        headers.setContentType(MediaType.APPLICATION_JSON);
    }
    @Test
    public void Test1_Initial(){

        TableRequest request = TableRequest.builder()
                .tableList(List.of(new TableRequest.Table(4,5)))
                .build();

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(request), headers);

        ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/table/initial"), HttpMethod.PUT, entity, Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void Test2_Reserve(){

        ReserveRequest request = ReserveRequest.builder()
                .customer(11)
                .build();

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(request), headers);

        ResponseEntity<ReserveResponse> response = restTemplate.exchange(createURLWithPort("/table/reserve"), HttpMethod.POST, entity, ReserveResponse.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(Long.valueOf(1), response.getBody().getBookingId());
        Assert.assertEquals(Integer.valueOf(3), response.getBody().getReserve().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(2), response.getBody().getRemaining().get(0).getAmount());

    }

    @Test
    public void Test3_ReserveCancel(){

        ReserveCancelRequest request = ReserveCancelRequest.builder()
                .bookingId(1L)
                .build();

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(request), headers);

        ResponseEntity<ReserveCancelResponse> response = restTemplate.exchange(createURLWithPort("/table/reserve/cancel"), HttpMethod.POST, entity, ReserveCancelResponse.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(Long.valueOf(1), response.getBody().getBookingId());
        Assert.assertEquals(Integer.valueOf(3), response.getBody().getReserveCancel().get(0).getAmount());
        Assert.assertEquals(Integer.valueOf(5), response.getBody().getRemaining().get(0).getAmount());

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}

